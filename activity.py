#determine if the year is a leap year
def is_leap_year(year):
  
  if year % 400 ==0:
    return True
  elif year % 100 ==0:
    return False
  elif year % 4 ==0:
    return True
  else:
    return False

def star(row,col):
  for i in range(row):
    for j in range(col):
      print("*", end="")
    print()

year = 0
while(year <= 0):
  try:
    year = int(input("Enter a year: "))
    if year <=0:
      print("Invalid input. Enter a positive integer.")
      continue
    elif is_leap_year(year):
      print(year, "is a leap year.")
    else:
      print(year, "is not a leap year.")
  except ValueError:
    print("Invalid input. Enter an integer.")
    continue

row,col = 0,0
while(row <= 0 or col <= 0):
  try:
    row = int(input("Enter the number of rows: "))
    col = int(input("Enter the number of columns: "))
    if row <= 0 or col <= 0:
      print("Invalid input. Enter a positive integer.")
      continue
    else:
      star(row,col)
  except ValueError:
    print("Invalid input. Enter an integer.")
    continue
  

  
  





